IMGBUILD(5)
===========

Name
----

IMGBUILD - Arch Linux image build description file

Synopsis
--------

IMGBUILD

Description
-----------

This manual page is about the syntax and semantics of 'IMGBUILD' files. Once an
IMGBUILD is written, the image can be built with linkman:makeimg[1].

An IMGBUILD file is a declarative definition of most aspects of an Arch Linux
disk image to be built with 'makeimg'. However, some aspects are controlled by
files other than the IMGBUILD file, usually in the same directory. See
linkman:makeimg[1] for details.

Options and Directives
----------------------

*imgname*::
	A name for the image. Mostly informational, potentially influences the
	output file name (see 'format').

*format (array)*::
	The output format of the disk image. Supported values are:

*(raw <SIZE> <PARTTYPE> <FILESYSTEM>)*:::
	Creates a single raw diskimage, `<imgname>.img`. The `SIZE` parameter is
	passed to linkarchman:truncate[1]. The `PARTTYPE` parameter is passed to
	linkarchman:parted[8], but only "msdos" or "gpt" are supported. The
	`FILESYSTEM` parameter can be anything for which a `mkfs.<FILESYSTEM>`
	executable exists. Parameters have to be in order, but any number of them
	can be omitted to use the defaults: `(raw 8G msdos ext4)`.

*(tar <COMPRESSION>)*:::
	Creates a compressed tarball, `<imgname>.tar.<COMPRESSION>`. As makeimg
	is modelled after linkarchman:makepkg[8], it uses its compression code
	and settings. See the `COMPRESS*` in linkarchman:makepkg.conf[5] for
	supported formats and tuning settings. The `COMPRESSION` parameter can
	be omitted, defaults to `zstd`.

*(dir)*:::
	Creates a directory, `<imgname>`. This is basically
	linkarchman:mkarchroot[1] with a few additions.

*(custom)*:::
	Output format and naming are defined by the `setup` and `cleanup`
	functions in the IMGBUILD.

*hostconf (array)*::
	Specify config items to be copied from the host building the image.
	Add 'keyring' to install the host's pacman keyring on the image.
	Otherwise the keyring must be installed manually via files or by running
	linkarchman:pacman-key[8] in the provision script.
	Add 'mirrorlist' to install the host's mirrorlist. Otherwise the
	mirrorlist must be installed manually by supplying a file or a patch to
	the default mirrorlist or some custom solution in the provision script.
	Default is to not use anything from the host.

*packages (array)*::
	The packages to install into the image. Passed to
	linkarchman:pacstrap[8].

*services (array)*::
	The systemd units to enable in the image. This happens as the very last
	step, so it can also be used to enable custom units that were created
	e.g. in the provisioning step.

In addition to the special variables documented here custom variables can be
defined at will, e.g. for use in templates.

Build Customization Functions
-----------------------------

All of the above variables such as `$imgname` and `$packages` are available for
use in the customization functions. In addition, makeimg defines the following
variables:

*PART*::
	The partition mounted to the image root. Only available in the
	`provision()` function.

*BLKDEV*::
	The block device that PART is on. Only available in the
	`provision()` function.

*imgroot*::
	The directory at which the `setup()` function has to prepare the chroot,
	e.g. the target of the base mount.

The customization functions are:

*setup()*::
	This function should prepare the working directory (located at
	`$imgroot`) so that `pacstrap` can be run. A setup function for a multi
	partition raw image for example would create the image, mount the root
	partition at `$imgroot`, the boot partition at `$imgroot/boot`, etc.
	The setup function might also be a good place to call
	linkarchman:genfstab[8].

*cleanup()*::
	This function should clean up any mounts or similar actions performed
	during setup. It is executed in the same context as the setup, so any
	variables set in `setup()` can be referenced.

*provision()*::
	Runs in the context of the image (chrooted). This function should
	perform all desired modifications to the image that cannot be
	expressed in any of the other directives.

See Also
--------

linkman:makeimg[1], linkarchman:makepkg[8], linkarchman:makepkg.conf[5],
linkarchman:pacstrap[8], linkarchman:mkarchroot[1]

Bugs
----

If you find a problem, please open an issue at
https://gitlab.com/anemos-io/makeimg.

Authors
-------

This project is not affiliated with Arch Linux.

Maintainer:

* Conrad Hoffmann <ch@bitfehler.net>
