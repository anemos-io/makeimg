# makeimg

**HEADS UP:** makeimg has [moved](https://sr.ht/~bitfehler/makeimg)!

Create Arch Linux images (disk image, tarball, or chroot)

* [Project homepage](https://anemos.io/projects/makeimg)
* [Man page](https://anemos.io/man/makeimg.1.html)
* [Example IMGBUILD](https://gitlab.com/anemos-io/makeimg/-/tree/master/example)

## Building

* Man pages: `make man` 
* Man pages (html): `make html`

## Installing

You really only need the script. But there is also an [AUR
package](https://aur.archlinux.org/packages/makeimg-git) which contains the
script and the man pages.
