MAN_PAGES  = doc/makeimg.1.gz doc/IMGBUILD.5.gz
HTML_PAGES = doc/makeimg.1.html doc/IMGBUILD.5.html

man: $(MAN_PAGES)

html: $(HTML_PAGES)

doc/%.gz: doc/%.asciidoc
	a2x --no-xmllint --asciidoc-opts="-f doc/asciidoc.conf" -d manpage -f manpage -D doc $<
	gzip doc/$*

doc/%.html: doc/%.asciidoc
	asciidoc -b html5 -f doc/asciidoc.conf -d manpage -o $@ --theme flask $<

clean:
	rm -f $(MAN_PAGES) $(HTML_PAGES)

.PHONY: man html clean
