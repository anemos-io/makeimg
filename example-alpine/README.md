# Alpine example IMGBUILD

This directory contains a simple example of an IMGBUILD using the
**experimental** support for building [Alpine Linux][1] images.

[1]: https://alpinelinux.org "Alpine Linux homepage"

This only means that the _output_ can be an Alpine Linux image. The images
still have to be built on an Arch Linux machine, as `makeimg` heavily relies on
Arch-specific tooling.

Make sure you have read the comments within it to understand all the caveats
that apply when building Alpine images. You should have also read the [man
page][2].

[2]: https://anemos.io/man/makeimg.1.html "makeimg man page"

The goal of this example is to provide a baseline image that is as close as
possible to what you would get from running the installer. But it is work in
progress. The resulting image is usable, but might still differ from a proper
install in unexpected ways!
